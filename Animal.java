public abstract class Animal {

    protected String name;
    private int animalCount;

    Animal(String name){
        this.name = name;
        animalCount++;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void swim(int distance){
        System.out.println(name + " проплыл " + distance + " метров!");
    }

    public void run(int distance){
        System.out.println(name + " пробежал " + distance + " метров!");
    }
}
