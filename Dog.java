public class Dog extends Animal {
    private final int MAX_RUN_DISTANCE = 500;
    private final int MAX_SWIM_DISTANCE = 10;

    private int dogCounter;

    Dog(String name) {
        super(name);
        dogCounter++;
    }

    Dog() {
        super("Бобик");
        dogCounter++;
    }

    public int getDogCounter() {
        return dogCounter;
    }

    @Override
    public void run(int distance) {
        if (distance > MAX_RUN_DISTANCE) {
            super.run(distance);
        } else {
            System.out.println(name + " не может столько пробежать");
        }
    }

    @Override
    public void swim(int distance) {
        if (distance > MAX_SWIM_DISTANCE) {
            super.swim(distance);
        } else {
            System.out.println(name + " не может столько проплыть");
        }
    }
}
