public class Cat extends Animal {
    private final int MAX_RUN_DISTANCE = 200;
    private final int MAX_SWIM_DISTANCE = 0;

    private int catCounter;

    Cat(String name) {
        super(name);
        catCounter++;
    }

    Cat() {
        super("Котик");
        catCounter++;
    }

    public int getCatCounter() {
        return catCounter;
    }

    @Override
    public void run(int distance) {
        if (distance > MAX_RUN_DISTANCE) {
            super.run(distance);
        } else {
            System.out.println(name + " не может столько пробежать");
        }
    }

    @Override
    public void swim(int distance) {
        if (distance > MAX_SWIM_DISTANCE) {
            super.swim(distance);
        } else {
            System.out.println(name + " не может столько проплыть");
        }
    }
}
